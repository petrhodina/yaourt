#!/bin/sh

YAOURT_URL="https://aur.archlinux.org/packages/ya/yaourt/yaourt.tar.gz"
PACKAGE_QUERY_URL="https://aur.archlinux.org/packages/pa/package-query/package-query.tar.gz"

YAOURT_TAR="yaourt.tar.gz"
PACKAGE_QUERY_TAR="package-query.tar.gz"

YAOURT_DIR="yaourt"
PACKAGE_QUERY_DIR="package-query"

echo "Downloading yaourt ..."

wget -O $YAOURT_TAR $YAOURT_URL

echo "Downloading package-query ..."

wget -O $PACKAGE_QUERY_TAR $PACKAGE_QUERY_URL

echo "Extracting yaourt ..."

tar xf $YAOURT_TAR 

echo "Extracting package-query ..."

tar xf $PACKAGE_QUERY_TAR

cd $PACKAGE_QUERY_DIR

echo "Creating package ..."

makepkg -s

echo "Installing package-query ..."
sudo pacman -U *.pkg.tar.xz

cd ../$YAOURT_DIR

echo "Creating package ..."

makepkg -s

echo "Installing yaourt ..."
sudo pacman -U *.pkg.tar.xz

cd ..

echo "Removing temporary files ..."

rm -r $YAOURT_DIR $YAOURT_TAR $PACKAGE_QUERY_DIR $PACKAGE_QUERY_TAR

echo "Updating package database ...."

yaourt -Suy

echo "Done"
